package linked_lists;

public class ReverseADoublyLinkedList {
    static DoublyLinkedListNode reverse(DoublyLinkedListNode head) {
        DoublyLinkedListNode pointer = head;

        while (true) {
            DoublyLinkedListNode temp = pointer.prev;
            pointer.prev = pointer.next;
            pointer.next = temp;

            if (pointer.prev == null) return pointer;
            else pointer = pointer.prev;
        }
    }

    class DoublyLinkedListNode {
        int data;
        DoublyLinkedListNode next;
        DoublyLinkedListNode prev;
    }
}
