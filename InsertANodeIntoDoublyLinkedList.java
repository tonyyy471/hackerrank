package linked_lists;

public class InsertANodeIntoDoublyLinkedList {
    static DoublyLinkedListNode sortedInsert(DoublyLinkedListNode head, int data) {
        if (head == null) return null;

        DoublyLinkedListNode pointer = head;
        while (true) {
            DoublyLinkedListNode newNode = new DoublyLinkedListNode(data);

            if (data <= pointer.data) {
                newNode.next = pointer;
                newNode.prev = pointer.prev;

                pointer.prev = newNode;

                if (newNode.prev != null) newNode.prev.next = newNode;
                else head = newNode;
                break;
            }

            if (pointer.next == null) {
                pointer.next = newNode;
                newNode.prev = pointer;
                break;
            }
            pointer = pointer.next;
        }

        return head;
    }


    private static class DoublyLinkedListNode {
        int data;
        DoublyLinkedListNode next;
        DoublyLinkedListNode prev;

        DoublyLinkedListNode(int data) {
            this.data = data;
        }
    }
}
