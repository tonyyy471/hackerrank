package greedy_algorithms;

import java.util.Arrays;

public class MaxMin {
    static int maxMin(int k, int[] arr) {
        Arrays.sort(arr);
        int[] minDiff = {Integer.MAX_VALUE};
        findMinDiff(arr, k, minDiff);
        return minDiff[0];
    }

    private static void findMinDiff(int[] arr, int k, int[] minDiff) {
        for (int i = 0; i <= arr.length - k; i++) {
            int currentMinDiff = arr[i + k - 1] - arr[i];

            if (currentMinDiffIsSmallerThanMinDIff(currentMinDiff, minDiff[0]))
                minDiff[0] = currentMinDiff;
        }
    }

    private static boolean currentMinDiffIsSmallerThanMinDIff(int currentMinDiff, int minDiff) {
        return currentMinDiff < minDiff;
    }
}
