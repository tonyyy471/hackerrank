package recursion_and_backtracking;

public class RecursiveDigitsSum {
    public static void main(String[] args) {
        System.out.println(superDigit("861568688536788", 10000));
    }

    static int superDigit(String number, int k) {
        if (number.length() > 1) {
            long sum = 0;
            for (int i = 0; i < number.length(); i++) {
                sum += Character.getNumericValue(number.charAt(i));
            }
            return superDigit(Long.toString(sum * k), 1);
        } else
            return Character.getNumericValue(number.charAt(0));
    }
}
