package search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SwapNodesAlgo {

    static int[][] swapNodes(int[][] indexes, int[] queries) {
        Node tree = extractTree(indexes);

        int numberOfNodes = indexes.length;
        int numberOfSwaps = queries.length;

        int[][] treeStatesAfterSwaps = new int[numberOfSwaps][numberOfNodes];

        swapNodesAndAddState(tree, treeStatesAfterSwaps, queries);

        return treeStatesAfterSwaps;
    }

    private static Node extractTree(int[][] indexes) {
        Node root = new Node(1);
        List<Node> currentLevel = Collections.singletonList(root);

        int index = 0;
        while (!currentLevel.isEmpty()) {
            List<Node> nextLevel = new ArrayList<>();
            for (Node parent : currentLevel) {
                if (parent != null) {
                    int newLeftChildValue = indexes[index][0];
                    int newRightChildValue = indexes[index][1];

                    parent.left = newLeftChildValue == -1 ? null : new Node(newLeftChildValue);
                    parent.right = newRightChildValue == -1 ? null : new Node(newRightChildValue);

                    nextLevel.add(parent.left);
                    nextLevel.add(parent.right);
                    index++;
                }
            }
            currentLevel = nextLevel;
        }

        return root;
    }

    private static void swapNodesAndAddState(Node root, int[][] treeStatesAfterEachSwap, int[] queries) {
        int treeStatesAfterEachSwapIndex = 0;

        for (int query : queries) {
            swapNodes(root, query);

            int numberOfNodes = treeStatesAfterEachSwap[0].length;
            int[] currentTreeState = new int[numberOfNodes];

            extractTreeStateInOrder(currentTreeState, root, new int[]{0});

            treeStatesAfterEachSwap[treeStatesAfterEachSwapIndex++] = currentTreeState;
        }
    }

    private static void swapNodes(Node root, int k) {
        List<Node> currentLevel = Collections.singletonList(root);

        int treeDepth = 1;
        while (!currentLevel.isEmpty()) {
            List<Node> nextLevel = new ArrayList<>();

            for (Node parent : currentLevel) {

                if (parent != null) {
                    if (isMultiple(treeDepth, k)) {
                        Node temp = parent.left;
                        parent.left = parent.right;
                        parent.right = temp;
                    }
                    nextLevel.add(parent.left);
                    nextLevel.add(parent.right);
                }
            }
            currentLevel = nextLevel;
            treeDepth++;
        }
    }

    private static boolean isMultiple(int numberOne, int numberTwo) {
        return numberOne % numberTwo == 0;
    }

    private static void extractTreeStateInOrder(int[] treeState, Node root, int[] index) {
        if (root.left != null) extractTreeStateInOrder(treeState, root.left, index);
        treeState[index[0]++] = root.value;
        if (root.right != null) extractTreeStateInOrder(treeState, root.right, index);
    }

    private static class Node {
        int value;
        Node left;
        Node right;

        Node(int value) {
            this.value = value;
        }
    }
}
