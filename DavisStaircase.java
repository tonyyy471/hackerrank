package recursion_and_backtracking;

public class DavisStaircase {

    static int stepPerms(int n) {
        int[] cache = new int[n + 1];

        return findSteps(n, cache);
    }

    private static int findSteps(int n, int[] cache) {
        if (n == 1) return 1;
        if (n == 2) return 2;
        if (n == 3) return 4;

        if (cache[n] == 0) cache[n] = findSteps(n - 1, cache) + findSteps(n - 2, cache) + findSteps(n - 3, cache);
        return cache[n];
    }
}
