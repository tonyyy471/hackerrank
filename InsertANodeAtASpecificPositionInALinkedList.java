package linked_lists;

public class InsertANodeAtASpecificPositionInALinkedList {
    static SinglyLinkedListNode insertNodeAtPosition(SinglyLinkedListNode head, int data, int position) {
        if (!hasNext(head)) return null;

        SinglyLinkedListNode pointer = head;
        int indexCounter = 0;

        while (hasNext(pointer)) {
            if (isIndexBeforePositionReached(indexCounter, position))
                insertAfter(pointer, new SinglyLinkedListNode(data));

            indexCounter++;
            pointer = pointer.next;
        }

        return head;
    }

    private static boolean hasNext(SinglyLinkedListNode node) {
        return node.next != null;
    }

    private static boolean isIndexBeforePositionReached(int indexCounter, int position) {
        return indexCounter == position - 1;
    }

    private static void insertAfter(SinglyLinkedListNode pointer, SinglyLinkedListNode newNode) {
        newNode.next = pointer.next;
        pointer.next = newNode;
    }

    private static class SinglyLinkedListNode {
        SinglyLinkedListNode(int data) {
            this.data = data;
        }

        int data;
        SinglyLinkedListNode next;
    }
}
