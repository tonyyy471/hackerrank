package string_manipulation;

import java.util.HashMap;
import java.util.Map;

public class MakingAnagrams {
    public static void main(String[] args) {
        System.out.println(makeAnagram("fcrxzwscanmligyxyvym", "jxwtrhvujlmrpdoqbisbwhmgpmeoke"));
    }

    static int makeAnagram(String a, String b) {
        Map<Character, Integer> aChars = new HashMap<>();
        fillMap(aChars, a);

        int[] similarChars = {0};
        compare(aChars, b, similarChars);

        return (a.length() - similarChars[0]) + (b.length() - similarChars[0]);
    }

    private static void fillMap(Map<Character, Integer> aChars, String a) {
        for (int i = 0; i < a.length(); i++) {
            addToMap(aChars, a.charAt(i));
        }
    }

    private static void addToMap(Map<Character, Integer> map, Character key) {
        if (map.containsKey(key)) map.put(key, map.get(key) + 1);
        else map.put(key, 1);
    }

    private static void compare(Map<Character, Integer> aChars, String b, int[] similarChars) {
        for (int i = 0; i < b.length(); i++) {
            char current = b.charAt(i);

            if (aChars.containsKey(current)) {
                if (aChars.get(current) > 0) similarChars[0]++;

                aChars.put(current, aChars.get(current) - 1);
            }
        }
    }
}
