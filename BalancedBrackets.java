package stacks_and_queues;

import java.util.Stack;

public class BalancedBrackets {
    static String isBalanced(String s) {
        if (isSLengthOdd(s)) return "NO";

        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < s.length(); i++) {
            char bracket = s.charAt(i);

            if (isOpening(bracket)) {
                stack.push(bracket);
            } else {
                if (stack.isEmpty()) return "NO";
                if (openingAndClosingMatch(stack.peek(), bracket)) {
                    stack.pop();
                } else {
                    return "NO";
                }
            }
        }

        return everyOpeningHasClosing(stack);
    }

    private static boolean isSLengthOdd(String s) {
        return s.length() % 2 != 0;
    }

    private static boolean isOpening(char bracket) {
        return isRegularOpening(bracket) || isRectangularOpening(bracket) || isCurlyOpening(bracket);
    }

    private static boolean openingAndClosingMatch(char opening, char closing) {
        if (isRegularOpening(opening) && isRegularClosing(closing)) return true;
        if (isRectangularOpening(opening) && isRectangularClosing(closing)) return true;
        if (isCurlyOpening(opening) && isCurlyClosing(closing)) return true;
        return false;
    }

    private static boolean isRegularOpening(char bracket) {
        return bracket == '(';
    }

    private static boolean isRegularClosing(char bracket) {
        return bracket == ')';
    }

    private static boolean isRectangularOpening(char bracket) {
        return bracket == '[';
    }

    private static boolean isRectangularClosing(char bracket) {
        return bracket == ']';
    }

    private static boolean isCurlyOpening(char bracket) {
        return bracket == '{';
    }

    private static boolean isCurlyClosing(char bracket) {
        return bracket == '}';
    }

    private static String everyOpeningHasClosing(Stack<Character> stack) {
        return stack.isEmpty() ? "YES" : "NO";
    }
}
