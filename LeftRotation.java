package arrays;

public class LeftRotation {
    static int[] rotLeft(int[] a, int d) {
        if (nothingToRotate(a, d)) return a;

        for (int i = 0; i < d; i++) {
            rotate(a);
        }
        return a;
    }

    private static boolean nothingToRotate(int[] a, int d) {
        return a.length == 0 || a.length == 1 || d == a.length;
    }

    private static void rotate(int[] a) {
        int firstElement = a[0];

        for (int i = 1; i < a.length; i++) {
            int currentElement = a[i];
            a[i - 1] = currentElement;
        }
        a[a.length - 1] = firstElement;
    }
}
