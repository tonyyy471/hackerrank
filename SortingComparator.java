package sorting;

import java.util.Arrays;
import java.util.Comparator;

public class SortingComparator implements Comparator<SortingComparator.Player> {
    @Override
    public int compare(Player o1, Player o2) {
        if (o1.score > o2.score) return -1;
        else if (o2.score > o1.score) return 1;
        else return compareStrings(o1.name, o2.name);
    }

    private int compareStrings(String o1, String o2) {
        for (int i = 0; i < Math.min(o1.length(), o2.length()); i++) {
            char o1Char = o1.charAt(i),
                    o2Char = o2.charAt(i);

            int charComparison = Character.compare(o1Char, o2Char);
            if (charComparison != 0) return charComparison;
        }
        return o1.length() > o2.length() ? 1 : -1;
    }

    static class Player {
        String name;
        int score;

        public Player(String name, int score) {
            this.name = name;
            this.score = score;
        }

        @Override
        public String toString() {
            return this.name + " " + this.score;
        }
    }
}
