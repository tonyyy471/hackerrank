package brickwork;

import java.util.HashMap;
import java.util.Map;

class InputVerifier {
    //Verifies that an input line has a specific length
    void verifyLength(int expected, int actual, String exceptionMessage) {
        if (!(expected == actual)) throw new IllegalArgumentException(exceptionMessage);
    }

    //Verifies that every element in the array is an integer
    void verifyContainsIntegers(String[] line) {
        for (String s : line) verifyIsInteger(s);
    }

    //Verifies that the element is an integer
    private void verifyIsInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Input contains a non integer value");
        }
    }

    //Verifies that every element in the array is an even integer
    void verifyValuesAreEven(int[] line) {
        for (int i : line) verifyIsEven(i);
    }

    //Verifies that the element is an even integer
    private void verifyIsEven(int i) {
        if (!(i % 2 == 0)) throw new IllegalArgumentException("Area size input contains a non even value");
    }

    //Verifies that every element in the array is an integer with a value less than 100
    void verifyValuesAreLessThanHundred(int[] line) {
        for (int i : line) verifyIsLessThanHundred(i);
    }

    //Verifies that the element is an integer with a value less than 100
    private void verifyIsLessThanHundred(int i) {
        if (i > 100) throw new IllegalArgumentException("Area size input contains a value more than hundred");
    }

    //Verifies that every value in the input area appears twice
    void verifyValuesAppearTwice(int[][] area) {
        Object[] values = toHashMap(area).values().toArray();

        for (Object brickSize : values)
            if (!((int) brickSize == 2))
                throw new IllegalArgumentException("Area input contains an invalid brick size");
    }

    //Returns a HashMap that contains the distinct values of the area as its keys, and the count that they appear as its values
    private Map<Integer, Integer> toHashMap(int[][] area) {
        Map<Integer, Integer> map = new HashMap<>();

        for (int[] areaLine : area)
            for (int brickValue : areaLine)
                if (map.containsKey(brickValue)) map.put(brickValue, map.get(brickValue) + 1);
                else map.put(brickValue, 1);

        return map;
    }
}
