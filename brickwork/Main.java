package brickwork;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        Brickwork brickwork = new Brickwork.BrickworkBuilder().build();
        brickwork.startBrickWork();
    }
}
