package brickwork;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

class Brickwork {
    //Instance of InputReader class that reads the input data
    private InputReader inputReader;
    //Int matrix that initially stores the first layer after its read, and then the second built layer
    private int[][] area;

    //Constructor is private, because there is an inner class providing new instance of the Brickwork class
    private Brickwork(InputReader inputReader) {
        this.inputReader = inputReader;
    }

    void startBrickWork() throws IOException {
        buildAreaFirstLayer();
        buildAreaSecondLayer();
        printArea();
    }

    //Reads and records the input into the area matrix
    private void buildAreaFirstLayer() throws IOException {
        area = inputReader.readArea();
    }

    //Builds the second layer of the area, and records it into the area matrix
    private void buildAreaSecondLayer() {
        //A list that contains the distinct values of the area, used to post the bricks of the second layer.
        //It guarantees that we use the right brick values in case that the first layer contains non consecutive values from 1 to (NxM)/2
        List<Integer> brickValues = brickValuesToList();
        int index = 0;

        //We iterate over the area, incrementing rows and columns by 2, with the idea to cover 2x2 area space at a time
        for (int i = 0; i < area.length; i += 2)
            for (int j = 0; j < area[i].length; j += 2) {

                //If either row of the 2x2 space is a valid horizontal brick, we put two vertical bricks.
                //This move guarantees that we have posted the bricks in a way that cannot turn out to be invalid, and we have arranged
                //that square area correctly. Now we are safe to continue to the next 2x2 square.
                if (isUpperHorizontalBrick(i, j) || isLowerHorizontalBrick(i, j)) {
                    postVerticalBrick(i, j, brickValues.get(index++));
                    postVerticalBrick(i, j + 1, brickValues.get(index++));
                    continue;
                }

                //If either col of the 2x2 space is a valid vertical brick, we put two horizontal bricks.
                //This move guarantees that we have posted the bricks in a way that cannot turn out to be invalid, and we have arranged
                //that square area correctly. Now we are safe to continue to the next 2x2 square.
                if (isLeftVerticalBrick(i, j) || isRightVerticalBrick(i, j)) {
                    postHorizontalBrick(i, j, brickValues.get(index++));
                    postHorizontalBrick(i + 1, j, brickValues.get(index++));
                    continue;
                }

                //If no valid horizontal or vertical brick appears in the first layer of the current 2x2 area that we are covering,
                //then we may safely post either two horizontal or two vertical bricks, and continue to the next 2x2 area.
                postHorizontalBrick(i, j, brickValues.get(index++));
                postHorizontalBrick(i + 1, j, brickValues.get(index++));
            }
    }

    //Checks if the upper two cells of the current 2x2 area have same values
    private boolean isUpperHorizontalBrick(int i, int j) {
        return area[i][j] == area[i][j + 1];
    }

    //Checks if the lower two cells of the current 2x2 area have same values
    private boolean isLowerHorizontalBrick(int i, int j) {
        return area[i + 1][j] == area[i + 1][j + 1];
    }

    //Checks if the left two cells of the current 2x2 area have same values
    private boolean isLeftVerticalBrick(int i, int j) {
        return area[i][j] == area[i + 1][j];
    }

    //Checks if the right two cells of the current 2x2 area have same values
    private boolean isRightVerticalBrick(int i, int j) {
        return area[i][j + 1] == area[i + 1][j + 1];
    }

    //Assigns equal values to two vertically adjacent cells in the area
    private void postVerticalBrick(int i, int j, int brickValue) {
        area[i][j] = brickValue;
        area[i + 1][j] = brickValue;
    }

    //Assigns equal values to two horizontally adjacent cells in the area
    private void postHorizontalBrick(int i, int j, int brickValue) {
        area[i][j] = brickValue;
        area[i][j + 1] = brickValue;
    }

    //Returns an ArrayList that contains the distinct values of the area
    private List<Integer> brickValuesToList() {
        Set<Integer> brickValues = new HashSet<>();

        for (int[] areaLine : area)
            for (int brickValue : areaLine) brickValues.add(brickValue);

        return new ArrayList<>(brickValues);
    }

    //Prints the stored area matrix
    private void printArea() {
        for (int[] areaLine : area)
            System.out.println(Arrays.toString(areaLine));
    }

    //A static inner class, that has only one method, used to build an instance of the Brickword class with its dependencies, and
    //their respective dependencies
    static class BrickworkBuilder {
        Brickwork build() {
            return new Brickwork(new InputReader(new BufferedReader(new InputStreamReader(System.in)), new InputVerifier()));
        }
    }
}
