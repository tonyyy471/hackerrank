package brickwork;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;

class InputReader {
    //Instance of BufferedReader class, that reads input from the keyboard
    private BufferedReader bufferedReader;
    //Instance of InputVerifier class, that verifies the input
    private InputVerifier inputVerifier;

    InputReader(BufferedReader bufferedReader, InputVerifier inputVerifier) {
        this.bufferedReader = bufferedReader;
        this.inputVerifier = inputVerifier;
    }

    //Reads and verifies the input area, then returns it as an int matrix
    int[][] readArea() throws IOException {
        int[] areaSize = readAreaSize();
        int height = areaSize[0], width = areaSize[1];

        int[][] area = new int[height][width];

        for (int i = 0; i < height; i++) area[i] = readAreaLine(width);

        inputVerifier.verifyValuesAppearTwice(area);

        return area;
    }

    //Reads and verifies the first input line, containing the size of the area, then returns it as an int array
    private int[] readAreaSize() throws IOException {
        String[] inputLine = bufferedReader.readLine().split(" ");

        inputVerifier.verifyLength(2, inputLine.length, "Area size input contains less or more than two values");
        inputVerifier.verifyContainsIntegers(inputLine);

        int[] areaSize = Arrays.stream(inputLine).mapToInt(Integer::parseInt).toArray();

        inputVerifier.verifyValuesAreLessThanHundred(areaSize);
        inputVerifier.verifyValuesAreEven(areaSize);

        return areaSize;
    }

    //Reads and verifies an input line, then returns it as an int array
    private int[] readAreaLine(int width) throws IOException {
        String[] inputLine = bufferedReader.readLine().split(" ");

        inputVerifier.verifyLength(width, inputLine.length, "Area line input is invalid");
        inputVerifier.verifyContainsIntegers(inputLine);

        return Arrays.stream(inputLine).mapToInt(Integer::parseInt).toArray();
    }
}