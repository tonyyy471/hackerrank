package arrays;

public class TwoDArrayDS {
    static int hourglassSum(int[][] arr) {
        int maxHourGlassSum = Integer.MIN_VALUE;

        for (int i = 0; i < arr.length - 2; i++) {
            for (int j = 0; j < arr[i].length - 2; j++) {
                int sum = calculateCurrentHourGlassSum(arr, i, j);
                if (sum > maxHourGlassSum) maxHourGlassSum = sum;
            }
        }

        return maxHourGlassSum;
    }

    private static int calculateCurrentHourGlassSum(int[][] arr, int i, int j) {
        return arr[i][j] + arr[i][j + 1] + arr[i][j + 2]
                + arr[i + 1][j + 1]
                + arr[i + 2][j] + arr[i + 2][j + 1] + arr[i + 2][j + 2];
    }
}
