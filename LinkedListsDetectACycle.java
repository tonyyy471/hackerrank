package linked_lists;

import java.util.ArrayList;
import java.util.List;

public class LinkedListsDetectACycle {
    public static void main(String[] args) {

    }

    boolean hasCycle(Node head) {
        if (head == null) return false;

        List<Node> nodes = new ArrayList<>();
        Node pointer = head;

        while (pointer != null) {
            if (nodes.contains(pointer)) return true;
            nodes.add(pointer);
            pointer = pointer.next;
        }

        return false;
    }

    private class Node {
        int data;
        Node next;
    }
}
