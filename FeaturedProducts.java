package dictionaries_and_hashmaps;

import java.util.*;

public class FeaturedProducts {
    public static String featuredProduct(List<String> products) {
        if (products.isEmpty()) return null;

        Map<String, Integer> map = new HashMap<>();

        for (int i = 0; i < products.size(); i++) {
            String product = products.get(i);
            fill(map, product);
        }

        Object[] values = map.values().toArray();
        int mostPurchases = getMostPurchasesValue(values);

        Object[] keys = map.keySet().toArray();
        List<String> mostPurchasedProducts = getAndSortMostPurchasedProducts(map, keys, mostPurchases);

        return mostPurchasedProducts.get(mostPurchasedProducts.size() - 1);
    }

    private static void fill(Map<String, Integer> map, String product) {
        if (map.containsKey(product)) map.put(product, map.get(product) + 1);
        else map.put(product, 1);
    }

    private static int getMostPurchasesValue(Object[] values) {
        int mostPurchases = 0;
        for (int i = 0; i < values.length; i++) {
            int currentProductPurchases = (int) values[i];
            if (currentProductPurchases > mostPurchases) mostPurchases = currentProductPurchases;
        }

        return mostPurchases;
    }

    private static List<String> getAndSortMostPurchasedProducts(Map<String, Integer> map, Object[] keys, int mostPurchases) {
        List<String> mostPurchasedProducts = new ArrayList<>();
        for (int i = 0; i < keys.length; i++) {
            String key = (String) keys[i];
            int purchases = map.get(key);

            if (purchases == mostPurchases) mostPurchasedProducts.add(key);
        }

        Collections.sort(mostPurchasedProducts);

        return mostPurchasedProducts;
    }
}
