package stacks_and_queues;

import java.util.Stack;

public class ATaleOfTwoStacks<T> {
    private Stack<T> newestOnTop = new Stack<>();
    private Stack<T> oldestOnTop = new Stack<>();

    public void enqueue(T value) {
        newestOnTop.push(value);
    }

    public T peek() {
        T value = getFirstElement(false);
        return value;
    }

    public T dequeue() {
        T value = getFirstElement(true);
        return value;
    }

    private T getFirstElement(boolean dequeue) {
        if (!oldestOnTop.isEmpty()) {
            if (dequeue) return oldestOnTop.pop();
            else return oldestOnTop.peek();
        } else {
            while (!newestOnTop.isEmpty()) {
                T current = newestOnTop.pop();
                oldestOnTop.push(current);
            }
            if (dequeue) return oldestOnTop.pop();
            else return oldestOnTop.peek();
        }
    }
}
