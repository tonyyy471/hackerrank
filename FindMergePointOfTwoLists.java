package linked_lists;

public class FindMergePointOfTwoLists {
    static int findMergeNode(SinglyLinkedListNode head1, SinglyLinkedListNode head2) {
        SinglyLinkedListNode pointer1 = head1,
                pointer2 = head2;

        while (pointer1 != null) {
            while (pointer2 != null) {
                if (pointer1 == pointer2) return pointer1.data;
                pointer2 = pointer2.next;
            }
            pointer2 = head2;
            pointer1 = pointer1.next;
        }
        return 0;
    }

    private class SinglyLinkedListNode {
        int data;
        SinglyLinkedListNode next;
    }
}
