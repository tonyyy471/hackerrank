package sorting;

public class BubbleSort {
    private static void bubbleSort(int[] a, int[] swaps) {
        while (true) {
            boolean swapped = false;
            for (int i = 0; i < a.length - 1; i++) {
                if (a[i] > a[i + 1]) {
                    swap(a, i);
                    swapped = true;
                    swaps[0]++;
                }
            }
            if (!swapped) break;
        }
    }

    private static void swap(int[] a, int i) {
        int temp = a[i];
        a[i] = a[i + 1];
        a[i + 1] = temp;
    }
}
